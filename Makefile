.PHONY: all django-admin-hub-channel django-admin-hub flask-apihub membership apihub gp ec tsp

all: django-admin-hub-channel django-admin-hub flask-apihub membership apihub gp ec tsp

clean:
		rm -rf dist/*

GIT_HASH := $(shell git rev-parse --short HEAD)
VER := $(shell echo ${GATEWAY_API_VER})

PKG_PATH := package/io/api-hub

django-admin-hub-channel: apihub
		cp dist/apihub/*.py django-admin-hub-channel/

django-admin-hub: membership gp
		cp dist/membership/*.py django-admin-hub/
		cp dist/gp/*.py django-admin-hub/

flask-apihub: membership apihub gp ec tsp
		cp dist/membership/*.py flask-apihub/
		cp dist/apihub/*.py flask-apihub/
		cp dist/tsp/*.py flask-apihub/
		cp dist/gp/*.py flask-apihub/
		cp dist/ec/*.py flask-apihub/

membership:
		mkdir -p dist/membership
		protoc -I=$(PKG_PATH)/membership -I=$(PKG_PATH)/core --python_out=dist/membership/ $(PKG_PATH)/membership/membership.proto $(PKG_PATH)/core/core.proto
		cp dist/membership/*.py rpc-membership/
		cp dist/apihub/*.py rpc-membership/

apihub:
		mkdir -p dist/apihub
		protoc -I=$(PKG_PATH)/apihub -I=$(PKG_PATH)/core --python_out=dist/apihub/ $(PKG_PATH)/apihub/apihub.proto $(PKG_PATH)/core/core.proto
		cp dist/apihub/*.py rpc-apihub/

gp:
		mkdir -p dist/gp
		protoc -I=$(PKG_PATH)/gp -I=$(PKG_PATH)/core --python_out=dist/gp/ $(PKG_PATH)/gp/gp.proto $(PKG_PATH)/core/core.proto
		cp dist/gp/*.py rpc-gp/

ec:
		mkdir -p dist/ec
		protoc -I=$(PKG_PATH)/ec -I=$(PKG_PATH)/core --python_out=dist/ec/ $(PKG_PATH)/ec/ec.proto $(PKG_PATH)/core/core.proto
		cp dist/ec/*.py rpc-ec/

tsp:
		mkdir -p dist/tsp
		protoc -I=$(PKG_PATH)/tsp -I=$(PKG_PATH)/core --python_out=dist/tsp/ $(PKG_PATH)/tsp/tsp.proto $(PKG_PATH)/core/core.proto
		cp dist/tsp/*.py rpc-tsp/
