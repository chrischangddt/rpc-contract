# rpc-contract
API Hub RPC Message and Service Schema

### 如何開始

- 請確認你至少已經安裝好 awscli 和 docker
- awscli
  - [https://pypi.org/project/awscli/](https://pypi.org/project/awscli/)
- docker for mac
  - [https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)


### 建立開發環境

```bash
docker-compose build protobuf
```

### 進入開發環境

```bash
docker-compose run protobuf
```

### 執行 Protocol Buffer Compiler

```bash
# Run make to compile all components
make

# Run for one of the compoennts
make membership

# See contents in dist/membership, dist/gp, etc
```


### 離開工作環境

```bash
# Execute exit in py-shell
exit

# Ctrl-D to terminal current session
```
